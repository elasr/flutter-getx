import 'package:get/get.dart';
import 'package:getx_master/routes/StateManagerPage.dart';
import 'package:getx_master/routes/StateManagerPage2.dart';

class GetPagesConfig {
  static final pages = [
    GetPage(name: "/state-manager", page: () => const StateManagerPage()),
    GetPage(name: "/state-manager2", page: () => const StateManagerPage2())
  ];
}
