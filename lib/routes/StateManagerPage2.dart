import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../data/CountController.dart';

class StateManagerPage2 extends StatefulWidget {
  const StateManagerPage2({Key? key}) : super(key: key);

  @override
  _StateManagerPage2State createState() => _StateManagerPage2State();
}

class _StateManagerPage2State extends State<StateManagerPage2> {
  // 找到GetX托管的其他页面使用的CountController实例。
  // 第一种写法：
  // final CountController controller = Get.find();
  // 第二种写法：
  // final CountController controller = Get.find<CountController>();
  // 第三种写法：
  final controller = Get.find<CountController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("状态管理-第二页"),
      ),
      body: ListView(
        children: [Obx(() => Text('${controller.count}'))],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          controller.decr();
        },
        child: const Icon(Icons.minimize),
      ),
    );
  }
}
