import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../data/CountController.dart';
import '../services/ApiService.dart';
import '../services/LocalStorageService.dart';

class StateManagerPage extends StatefulWidget {
  const StateManagerPage({Key? key}) : super(key: key);

  @override
  _StateManagerPageState createState() => _StateManagerPageState();
}

class _StateManagerPageState extends State<StateManagerPage> {
  // 直接使用Get.find<CountController>()即可，GetX会自动实例化并添加到GetX依赖管理中。
  CountController countController = Get.find<CountController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // 使用翻译，只需在字段后面加上.tr即可。
      appBar: AppBar(title: Text("hello".tr)),
      body: ListView(
        children: [
          Text("hello".tr),
          ElevatedButton(
              onPressed: () => Get.updateLocale(const Locale("de", "DE")),
              child: const Text("切换到德语")),
          ElevatedButton(
              onPressed: () => Get.updateLocale(const Locale("zh", "CN")),
              child: const Text("切换到中文")),
          ElevatedButton(
              // 因为没配置这个语言，所以使用原来的。
              onPressed: () => Get.updateLocale(const Locale("en", "US")),
              child: const Text("切换到英语")),
          ElevatedButton(
              onPressed: () {
                print(Get.find<ApiService>().BaseURL);
                // https://blogs.theworldofhua.zone
              },
              child: const Text("输出博客地址")),
          ElevatedButton(
              onPressed: () {
                Get.find<LocalStorageService>().setCount();
              },
              child: const Text("本地count")),
        ],
      ),
    );
  }
}
