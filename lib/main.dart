import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_master/data/Translation.dart';
import './router.dart';
import './AllController.dart';
import './services/ApiService.dart';
import './services/LocalStorageService.dart';

void main() async {
  await initApiService();
  await initLocalStorage();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      getPages: GetPagesConfig.pages,
      initialRoute: "/state-manager",
      initialBinding: AllController(),
      translations: Translation(), // 使用翻译配置
      locale: const Locale("zh", "CN"), // 设置默认语言
      // fallbackLocale: const Locale("en", "US"), // 上面指定的翻译不存在将使用这个。
    );
  }
}

// 外部调用异步函数时，需要将返回值定义为Future<void>
Future<void> initApiService() async {
  print("开始初始化Apis服务！");
  await Get.putAsync(() async => ApiService());
  print("初始化Apis服务完成！");
}

initLocalStorage() async {
  print("开始初始化本地存储服务！");
  await Get.putAsync(() async => LocalStorageService());
  print("初始化本地存储服务完成！");
  return;
}
