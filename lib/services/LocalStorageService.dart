import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageService extends GetxService {
  setCount() async {
    getPrefs().then((prefs) {
      int? _count = prefs.getInt("count");

      if (_count == null) {
        prefs.setInt("count", 0);
        print("本地count为空。");
      } else {
        print(_count);
        prefs.setInt("count", _count + 1);
      }
    });
  }
}

Future<SharedPreferences> getPrefs() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs;
}
