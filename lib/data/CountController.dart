import 'package:get/get.dart';

class CountController extends GetxController {
  final count = 0.obs;

  void incr() {
    count.value++;
  }

  void decr() {
    count.value--;
  }
}
