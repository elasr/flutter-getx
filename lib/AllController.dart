import 'package:get/get.dart';
import './data/Person.dart';
import './data/CountController.dart';

class AllController extends Bindings {
  @override
  void dependencies() {
    // TODO: implement dependencies

    // 只有当使用到的时候才会被实例化，添加到GetX依赖管理中。
    Get.lazyPut<Person>(() => Person());
    Get.lazyPut<CountController>(() => CountController());
  }
}
